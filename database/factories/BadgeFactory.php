<?php

namespace Database\Factories;

use App\Models\Badge;
use Illuminate\Database\Eloquent\Factories\Factory;

class BadgeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Badge::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'xp' => $this->faker->numberBetween($min = 100, $max = 5000),
            'description' => $this->faker->text(400),
            'icon' => 'badgeIcon/'.$this->faker->image('public/storage/badgeIcon',400,300, null, false),
        ];
    }
}
