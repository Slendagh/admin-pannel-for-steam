<?php

namespace Database\Factories;

use App\Models\Purchase;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Game;
use App\Models\User;

class PurchaseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Purchase::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $userId = User::pluck('id')->toArray();
        $gameId = Game::pluck('id')->toArray();
        return [
            'user_id' => $this->faker->randomElement($userId),
            'game_id' => $this->faker->randomElement($gameId),
            'date_of_purchase' => $this->faker->dateTime($max = 'now', $timezone = null),
        ];
    }
}
