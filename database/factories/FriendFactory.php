<?php

namespace Database\Factories;

use App\Models\Friend;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class FriendFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Friend::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $userId1 = User::pluck('id')->toArray();
        $userId2 = User::pluck('id')->toArray();
        return [
            'user1_id' => $this->faker->randomElement($userId1),
            'user2_id' => $this->faker->randomElement($userId2),
        ];
    }
}
