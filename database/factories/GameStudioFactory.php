<?php

namespace Database\Factories;

use App\Models\GameStudio;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Game;

class GameStudioFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = GameStudio::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $gameId = Game::pluck('id')->toArray();
        return [
            'game_id' => $this->faker->randomElement($gameId),
            'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'about' => $this->faker->text(400),
        ];
    }
}
