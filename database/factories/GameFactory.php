<?php

namespace Database\Factories;

use App\Models\Game;
use App\Models\Genre;
use App\Models\Category;
use App\Models\Tag;
use App\Models\AgeRating;
use App\Models\Publisher;
use App\Models\Developer;
use Illuminate\Database\Eloquent\Factories\Factory;

class GameFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Game::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $genreId = Genre::pluck('id')->toArray();
        $categoryId = Category::pluck('id')->toArray();
        $tagId = Tag::pluck('id')->toArray();
        $ageRatingId = AgeRating::pluck('id')->toArray();
        $publisherId = Publisher::pluck('id')->toArray();
        $developerId = Developer::pluck('id')->toArray();

        return [
            'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'genre_id' => $this->faker->randomElement($genreId),
            'category_id' => $this->faker->randomElement($categoryId),
            'tag_id' => $this->faker->randomElement($tagId),
            'description' => $this->faker->text(400),
            'age_rating_id' => $this->faker->randomElement($ageRatingId),
            'rating' => $this->faker->numberBetween($min = 1, $max = 5),
            'publisher_id' => $this->faker->randomElement($publisherId),
            'developer_id' => $this->faker->randomElement($developerId),
            'release_date' => $this->faker->dateTime($max = 'now', $timezone = null),
            'default_image' => 'gameImages/'.$this->faker->image('public/storage/gameImages',400,300, null, false),
            'images' => 'gameImages/'.$this->faker->image('public/storage/gameImages',400,300, null, false).','.'gameImages/'.$this->faker->image('public/storage/gameImages',400,300, null, false).','.'gameImages/'.$this->faker->image('public/storage/gameImages',400,300, null, false), //needs array of images !!!!
        ];
    }
}
