<?php

namespace Database\Factories;

use App\Models\UserBadgesActivity;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;
use App\Models\Badge;

class UserBadgesActivityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserBadgesActivity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $badgeId = Badge::pluck('id')->toArray();
        $userId = User::pluck('id')->toArray();
        return [
            'badge_id' => $this->faker->randomElement($badgeId),
            'user_id' => $this->faker->randomElement($userId),
            'date_of_unlock' => $this->faker->dateTime($max = 'now', $timezone = null),
        ];
    }
}
