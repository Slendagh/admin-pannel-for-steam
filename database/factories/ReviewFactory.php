<?php

namespace Database\Factories;

use App\Models\Review;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Game;
use App\Models\User;

class ReviewFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Review::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $userId = User::pluck('id')->toArray();
        $gameId = Game::pluck('id')->toArray();
        return [
            'user_id' => $this->faker->randomElement($userId),
            'game_id' => $this->faker->randomElement($gameId),
            'review' => $this->faker->text(400),
        ];
    }
}
