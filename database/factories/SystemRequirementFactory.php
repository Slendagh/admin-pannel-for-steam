<?php

namespace Database\Factories;

use App\Models\SystemRequirement;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Game;

class SystemRequirementFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SystemRequirement::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $gameId = Game::pluck('id')->toArray();
        return [
            'game_id' =>  $this->faker->randomElement($gameId),
            'storage_KB' => $this->faker->numberBetween($min = 100, $max = 5000),
        ];
    }
}
