<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tag;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = [
            [
                'name' => 'Looter Shooter',
            ],
            [
                'name' => 'Indie',
            ],
            [
                'name' => 'Action',
            ],
            [
                'name' => 'Adventure',
            ],
            [
                'name' => 'Casual', 
            ],
            [
                'name' => 'Simulation',
            ],
            [
                'name' => 'RPG',
            ],
            [
                'name' => 'Early Access',
            ],
            [
                'name' => 'Single Player',
            ],
            [
                'name' => 'Free to Play',
            ],
            [
                'name' => 'Violent',
            ],
            [
                'name' => 'Sports',
            ],
            [
                'name' => '2D',
            ],
            [
                'name' => 'Racing',
            ],
            [
                'name' => 'Multiplayer',
            ],
            [
                'name' => 'Puzzle',
            ],
            [
                'name' => 'Anime',
            ],
            [
                'name' => 'Story Rich',
            ],
            [
                'name' => 'VR',
            ]
        ];

        foreach ($tags as $tag) {
            Tag::create($tag);
        }
    }
}
