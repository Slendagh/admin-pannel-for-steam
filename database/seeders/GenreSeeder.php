<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Genre;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genres = [
            [
                'name' => 'Free to Play',
            ],
            [
                'name' => 'Early Access',
            ],
            [
                'name' => 'Action',
            ],
            [
                'name' => 'Adventure',
            ],
            [
                'name' => 'Casual', 
            ],
            [
                'name' => 'Indie',
            ],
            [
                'name' => 'Massively Multiplayer',
            ],
            [
                'name' => 'Racing',
            ],
            [
                'name' => 'RPG',
            ],
            [
                'name' => 'Simulation',
            ],
            [
                'name' => 'Sports',
            ],
            [
                'name' => 'Strategy',
            ]
        ];

        foreach ($genres as $genre) {
            Genre::create($genre);
        }
    }
}
