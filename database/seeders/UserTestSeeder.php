<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserTest;

class UserTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userTests=[
            [
                'name' => 'jess',
                'username' => 'jessie',
                'about' => 'some text',
                'email' => 'jess@gmail.com'
            ],
            [
                'name' => 'Michael',
                'username' => 'mick',
                'about' => 'some text',
                'email' => 'mick@gmail.com'
            ]
        ];

        foreach($userTests as $userTest){
            UserTest::create($userTest);
        }
    }
}
