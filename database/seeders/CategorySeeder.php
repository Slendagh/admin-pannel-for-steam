<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Top Seller',
            ],
            [
                'name' => 'New Releases',
            ],
            [
                'name' => 'Upcoming',
            ],
            [
                'name' => 'Adventure',
            ],
            [
                'name' => 'Specials', 
            ],
            [
                'name' => 'Virtual Reality',
            ],
            [
                'name' => 'Controller Friendly',
            ],
            [
                'name' => 'Racing',
            ]
        ];

        foreach ($categories as $category) {
            Category::create($category);
        }
    }
}
