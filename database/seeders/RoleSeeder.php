<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::create([
                'name' => 'Developer',
                'slug' => 'developer',
                'permissions' => json_encode([
                    'list-ones-games'=>true,
                    'edit-ones-games'=>true,
                    'list-ones-game-reviews'=>true
                ]),
        ]);

    }
}
