<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tutorial;

class TutorialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tutorials=[
            [
                'name' => 'jess',
                'username'=>'jessie',
                'email' => 'jess@gmail.com'
            ],
            [
            'name' => 'Michael',
            'username'=>'Mick',
            'email' => 'michael@gmail.com'
            ]
        ];

        foreach($tutorials as $tutorial){
            Tutorial::create($tutorial);
        }
    }
}
