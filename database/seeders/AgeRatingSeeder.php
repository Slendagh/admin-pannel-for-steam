<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AgeRating;

class AgeRatingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $AgeRatings = [
            [
                'name' => 'Rating Pending',
                'icon' => 'ageRatingIcons/pending.png',
                'description' => 'This symbol is used in promotional materials for games which have not yet been assigned a final rating by the ESRB'
            ],
            [
                'name' => 'Early Childhood',
                'icon' => 'ageRatingIcons/earlyChildhood.png',
                'description' => 'Games with this rating contain content which is aimed towards a preschool audience. They do not contain content that parents would find objectionable to this audience.'
            ],
            [
                'name' => 'Everyone',
                'icon' => 'ageRatingIcons/everyone.png',
                'description' => 'Games with this rating contain content which the ESRB believes is "generally suitable for all ages".'
            ],
            [
                'name' => 'Everyone 10+',
                'icon' => 'ageRatingIcons/everyone10.png',
                'description' => 'Games with this rating contain content which the ESRB believes is generally suitable for those aged 10 years and older.'
            ],
            [
                'name' => 'Teen', 
                'icon' => 'ageRatingIcons/teen.png',
                'description' => 'Games with this rating contain content which the ESRB believes is generally suitable for those aged 13 years and older'
            ],
            [
                'name' => 'Mature',
                'icon' => 'ageRatingIcons/mature.png',
                'description' => 'Games with this rating contain content which the ESRB believes is generally suitable for those aged 17 years and older'
            ],
            [
                'name' => 'Adults Only',
                'icon' => 'ageRatingIcons/adults.png',
                'description' => 'Games with this rating contain content which the ESRB believes is only suitable for those aged 18 years and older'
            ]
        ];

        foreach ($AgeRatings as $AgeRating) {
            AgeRating::create($AgeRating);
        }
    }
}
