<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //\App\Models\User::factory(10)->create();
       // \App\Models\Badge::factory(10)->create();
        //\App\Models\Developer::factory(10)->create();
        //\App\Models\Friend::factory(10)->create();
        //\App\Models\Publisher::factory(10)->create();
        //$this->call(AgeRatingSeeder::class);
        //$this->call(TagSeeder::class);
        //$this->call(GenreSeeder::class);
        //$this->call(CategorySeeder::class);
        //\App\Models\Game::factory(10)->create();
        //\App\Models\Purchase::factory(10)->create();
        //\App\Models\Review::factory(10)->create();
        //\App\Models\SystemRequirement::factory(10)->create();
        //\App\Models\UserBadgesActivity::factory(10)->create();
        //\App\Models\GameStudio::factory(10)->create();
        //$this->call(TutorialSeeder::class);
        $this->call(RoleSeeder::class);
    }
}
