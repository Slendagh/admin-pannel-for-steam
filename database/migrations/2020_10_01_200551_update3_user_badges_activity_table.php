<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update3UserBadgesActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_badges_activity', function (Blueprint $table) {
            $table->foreignId('user_id')->constraint('users')->change();
            $table->foreignId('badge_id')->constraint('badges')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_badges_activity', function (Blueprint $table) {
            $table->dropColumn(['user_id', 'badge_id']);
        });
    }
}
