<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update1UserBadgesActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_badges_activity', function (Blueprint $table) {
            $table->foreignId('badge_id')->constraint();
            $table->date('date_of_unlock');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_badges_activity', function (Blueprint $table) {
            $table->dropColumn(['badge_id', 'date_of_unlock']);
        });
    }
}
