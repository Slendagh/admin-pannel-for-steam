<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update5UserBadgesActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('user_badges_activity', 'user_badges_activities');
    }   

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::rename('user_badges_activities', 'user_badges_activity');
    }   
}
