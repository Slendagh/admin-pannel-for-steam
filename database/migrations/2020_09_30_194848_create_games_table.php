<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('title');
            $table->foreignId('genre_id')->constrained();
            $table->foreignId('category_id')->constrained();
            $table->foreignId('tag_id')->constrained();
            $table->longText('description');
            $table->foreignId('age_rating_id')->constrained();
            $table->integer('rating');
            $table->foreignId('publisher_id')->constrained();
            $table->foreignId('developer_id')->constrained();
            $table->date('release_date');
            $table->string('default_image');
            $table->longText('images')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}

