<?php

declare(strict_types=1);

use App\Orchid\Screens\Examples\ExampleCardsScreen;
use App\Orchid\Screens\Examples\ExampleChartsScreen;
use App\Orchid\Screens\Examples\ExampleFieldsAdvancedScreen;
use App\Orchid\Screens\Examples\ExampleFieldsScreen;
use App\Orchid\Screens\Examples\ExampleLayoutsScreen;
use App\Orchid\Screens\Examples\ExampleScreen;
use App\Orchid\Screens\Examples\ExampleTextEditorsScreen;
use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

use App\Orchid\Screens\GameEditScreen;
use App\Orchid\Screens\GameListScreen;
use App\Orchid\Screens\AgeRatingListScreen;
use App\Orchid\Screens\AgeRatingEditScreen;
use App\Orchid\Screens\BadgeListScreen;
use App\Orchid\Screens\BadgeEditScreen;
use App\Orchid\Screens\CategoryListScreen;
use App\Orchid\Screens\CategoryEditScreen;
use App\Orchid\Screens\DeveloperListScreen;
use App\Orchid\Screens\DeveloperEditScreen;
use App\Orchid\Screens\FriendListScreen;
use App\Orchid\Screens\FriendEditScreen;
use App\Orchid\Screens\GameStudioEditScreen;
use App\Orchid\Screens\GameStudioListScreen;
use App\Orchid\Screens\GenreListScreen;
use App\Orchid\Screens\GenreEditScreen;
use App\Orchid\Screens\PublisherListScreen;
use App\Orchid\Screens\PublisherEditScreen;
use App\Orchid\Screens\PurchaseEditScreen;
use App\Orchid\Screens\PurchaseListScreen;
use App\Orchid\Screens\ReviewListScreen;
use App\Orchid\Screens\ReviewEditScreen;
use App\Orchid\Screens\SystemRequirementListScreen;
use App\Orchid\Screens\SystemRequirementEditScreen;
use App\Orchid\Screens\TagListScreen;
use App\Orchid\Screens\TagEditScreen;
use App\Orchid\Screens\UserBadgeActivityListScreen;
use App\Orchid\Screens\UserBadgeActivityEditScreen;
use App\Orchid\Screens\TutorialListScreen;
use App\Orchid\Screens\TutorialEditScreen;






/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
Route::screen('/main', PlatformScreen::class)
    ->name('platform.main');

// Platform > Profile
Route::screen('profile', UserProfileScreen::class)
    ->name('platform.profile')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Profile'), route('platform.profile'));
    });

// Platform > System > Users
Route::screen('users/{users}/edit', UserEditScreen::class)
    ->name('platform.systems.users.edit')
    ->breadcrumbs(function (Trail $trail, $user) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Edit'), route('platform.systems.users.edit', $user));
    });

// Platform > System > Users > User
Route::screen('users', UserListScreen::class)
    ->name('platform.systems.users')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.index')
            ->push(__('Users'), route('platform.systems.users'));
    });

// Platform > System > Roles > Role
Route::screen('roles/{roles}/edit', RoleEditScreen::class)
    ->name('platform.systems.roles.edit')
    ->breadcrumbs(function (Trail $trail, $role) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Role'), route('platform.systems.roles.edit', $role));
    });

// Platform > System > Roles > Create
Route::screen('roles/create', RoleEditScreen::class)
    ->name('platform.systems.roles.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Create'), route('platform.systems.roles.create'));
    });

// Platform > System > Roles
Route::screen('roles', RoleListScreen::class)
    ->name('platform.systems.roles')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.index')
            ->push(__('Roles'), route('platform.systems.roles'));
    });

// Example...
Route::screen('example', ExampleScreen::class)
    ->name('platform.example')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Example screen'));
    });

Route::screen('example-fields', ExampleFieldsScreen::class)->name('platform.example.fields');
Route::screen('example-layouts', ExampleLayoutsScreen::class)->name('platform.example.layouts');
Route::screen('example-charts', ExampleChartsScreen::class)->name('platform.example.charts');
Route::screen('example-editors', ExampleTextEditorsScreen::class)->name('platform.example.editors');
Route::screen('example-cards', ExampleCardsScreen::class)->name('platform.example.cards');
Route::screen('example-advanced', ExampleFieldsAdvancedScreen::class)->name('platform.example.advanced');

Route::screen('user/create', UserEditScreen::class)
    ->name('platform.systems.users.create');

Route::screen('game/{id?}', GameEditScreen::class)
    ->name('platform.game.edit');

Route::screen('games', GameListScreen::class)
    ->name('platform.game.list');

Route::screen('ageratings', AgeRatingListScreen::class)
    ->name('platform.agerating.list');

Route::screen('agerating/{id?}', AgeRatingEditScreen::class)
    ->name('platform.agerating.edit');

Route::screen('badges', BadgeListScreen::class)
    ->name('platform.badge.list');

Route::screen('badge/{id?}', BadgeEditScreen::class)
    ->name('platform.badge.edit');

Route::screen('categories', CategoryListScreen::class)
    ->name('platform.category.list');

Route::screen('category/{id?}', CategoryEditScreen::class)
    ->name('platform.category.edit');

Route::screen('developers', DeveloperListScreen::class)
    ->name('platform.developer.list');

Route::screen('developer/{id?}', DeveloperEditScreen::class)
    ->name('platform.developer.edit');

Route::screen('friends', FriendListScreen::class)
    ->name('platform.friend.list');

Route::screen('friend/{id?}', FriendEditScreen::class)
    ->name('platform.friend.edit');

Route::screen('gameStudios', GameStudioListScreen::class)
    ->name('platform.gameStudio.list');

Route::screen('gameStudio/{id?}', GameStudioEditScreen::class)
    ->name('platform.gameStudio.edit');
    
Route::screen('genres', GenreListScreen::class)
    ->name('platform.genre.list');
    
Route::screen('genre/{id?}', GenreEditScreen::class)
    ->name('platform.genre.edit');

Route::screen('publishers', PublisherListScreen::class)
    ->name('platform.publisher.list');
    
Route::screen('publisher/{id?}', PublisherEditScreen::class)
->name('platform.publisher.edit');

Route::screen('purchases', PurchaseListScreen::class)
    ->name('platform.purchase.list');
    
Route::screen('purchase/{id?}', PurchaseEditScreen::class)
->name('platform.purchase.edit');

Route::screen('reviews', ReviewListScreen::class)
    ->name('platform.review.list');
    
Route::screen('review/{id?}', ReviewEditScreen::class)
->name('platform.review.edit');

Route::screen('systemRequirements', SystemRequirementListScreen::class)
    ->name('platform.systemRequirement.list');
    
Route::screen('systemRequirement/{id?}', SystemRequirementEditScreen::class)
    ->name('platform.systemRequirement.edit');

Route::screen('tags', TagListScreen::class)
    ->name('platform.tag.list');
    
Route::screen('tag/{id?}', TagEditScreen::class)
    ->name('platform.tag.edit');

Route::screen('badgeActivities', UserBadgeActivityListScreen::class)
    ->name('platform.userBadgeActivity.list');
    
Route::screen('badgeActivity/{id?}', UserBadgeActivityEditScreen::class)
    ->name('platform.userBadgeActivity.edit');

Route::screen('tutorials', TutorialListScreen::class)
    ->name('platform.tutorial.list');
    
Route::screen('tutorial/{id?}', TutorialEditScreen::class)
    ->name('platform.tutorial.edit');

//Route::screen('idea', 'Idea::class','platform.screens.idea');
