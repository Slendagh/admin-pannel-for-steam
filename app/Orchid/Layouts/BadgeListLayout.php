<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Screen\Actions\Link;
use app\Models\Badge;

class BadgeListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'badges';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id', 'ID')
            ->render(function (Badge $badge) {
                return Link::make($badge->id)
                    ->route('platform.badge.edit', $badge->id);
            }),
            TD::set('name','Name')
                ->width('250px'),
            TD::set('xp','XP needed'),
            TD::set('description', 'Description')
                ->width('300px'),
            TD::set('icon','Icon')
                ->render(function (Badge $badge) {
                    if(strpos($badge->icon, "badge") !== false){
                        $url = 'http://steam.slendagh.local/storage/'.$badge->icon;
                    }else{
                        $url = $badge->icon;
                    }
                    return "<img src=".$url." style='width:30px'>";
                }),
        ];
    }
}
