<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\TD;
use app\Models\Game;

class GameListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'games';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id', 'ID')
                ->render(function ($game) {
                    return Link::make($game->id)
                        ->route('platform.game.edit', $game->id);
                }),
            TD::set('title','Title')
                ->width('250px'),
            TD::set('description', 'Description')
                ->width('300px'),
            TD::set('genre_id', 'Genre'),
            TD::set('category_id', 'Category'),
            TD::set('tag_id', 'Tag'),
            TD::set('age_rating_id', 'Age Rating'),
            TD::set('rating', 'Rating'),
            TD::set('publisher_id', 'Publisher'),
            TD::set('developer_id', 'Developer'),
            TD::set('release_date','Date of release'),
            TD::set('default_image','Default Image')
            ->render(function ($game) {
                $url = 'http://steam.slendagh.local/storage/'.$game->default_image;
                return "<img src=".$url." style='width:30px'>";
            }),
        ];
    }
}
