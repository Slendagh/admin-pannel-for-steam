<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Screen\Actions\Link;
use App\Models\UserBadgesActivity;

class UserBadgeActivityListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'activities';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id', 'ID')
            ->render(function (UserBadgesActivity $activity) {
                return Link::make($activity->id)
                    ->route('platform.userBadgeActivity.edit', $activity->id);
            }),
            TD::set('badge_id', 'Badge Id'),
            TD::set('date_of_unlock', 'Date of Unlock'),
            TD::set('user_id', 'User Id'),

        ];
    }
}
