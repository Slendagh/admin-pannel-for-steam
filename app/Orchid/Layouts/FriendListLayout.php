<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Screen\Actions\Link;
use App\Models\Friend;

class FriendListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'friends';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('user1_id', 'User 1')
            ->render(function (Friend $friend) {
                return Link::make($friend->user1_id)
                    ->route('platform.friend.edit', $friend->user1_id);
            }),
            TD::set('user2_id', 'User 2')
            ->render(function (Friend $friend) {
                return Link::make($friend->user2_id)
                    ->route('platform.friend.edit', $friend->user2_id);
            }),
        ];
    }
}
