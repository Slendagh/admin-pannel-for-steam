<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Screen\Actions\Link;
use App\Models\Tutorial;

class TutorialListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'tutorials';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id', 'ID')
                ->render(function(Tutorial $tuto){
                    return Link::make($tuto->id)
                        ->route('platform.tutorial.edit', $tuto->id);
                }),
            TD::set('name', 'Name'),
            TD::set('username', 'Username'),
            TD::set('email', 'Email'),
        ];
    }
}
