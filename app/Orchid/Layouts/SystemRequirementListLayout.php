<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Screen\Actions\Link;
use App\Models\SystemRequirement;


class SystemRequirementListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'systemRequirements';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id', 'ID')
            ->render(function (SystemRequirement $sysRequirmnt) {
                return Link::make($sysRequirmnt->id)
                    ->route('platform.systemRequirement.edit', $sysRequirmnt->id);
            }),
            TD::set('game_id', 'Game ID'),
            TD::set('storage_KB', 'Needed Storage'),
        ];
    }
}
