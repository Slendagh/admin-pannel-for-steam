<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Screen\Actions\Link;
use app\Models\AgeRating;


class AgeRatingListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'AgeRatings';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id', 'ID')
            ->render(function (AgeRating $AgeRating) {
                return Link::make($AgeRating->id)
                    ->route('platform.agerating.edit', $AgeRating->id);
            }),
            TD::set('name','Name')
                ->width('250px'),
            TD::set('description', 'Description')
                ->width('300px'),
            TD::set('icon','Icon')
                ->render(function ($AgeRating) {
                    if(strpos($AgeRating->icon, "ageRating") !== false){
                        $url = 'http://steam.slendagh.local/storage/'.$AgeRating->icon;
                    }else{
                        $url = $AgeRating->icon;
                    }
                    return "<img src=".$url." style='width:30px'>";
                }),
        ];
    }
}
