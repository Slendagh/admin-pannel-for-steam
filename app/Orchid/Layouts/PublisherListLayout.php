<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Screen\Actions\Link;
use App\Models\Publisher;

class PublisherListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'publishers';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id', 'ID')
            ->render(function (Publisher $publisher) {
                return Link::make($publisher->id)
                    ->route('platform.publisher.edit', $publisher->id);
            }),
            TD::set('name', 'Name'),
            TD::set('about', 'About')
                ->width('250px'),
            TD::set('email', 'Email'),
        ];
    }
}
