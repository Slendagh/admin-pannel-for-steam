<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Screen\Actions\Link;
use App\Models\Review;

class ReviewListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'reviews';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id', 'ID')
            ->render(function (Review $review) {
                return Link::make($review->id)
                    ->route('platform.review.edit', $review->id);
            }),
            TD::set('user_id', 'User ID'),
            TD::set('game_id', 'Game ID'),
            TD::set('review', 'Review')
                ->width('300px'),
        ];
    }
}
