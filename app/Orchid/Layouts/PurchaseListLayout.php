<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Screen\Actions\Link;
use App\Models\Purchase;


class PurchaseListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'purchases';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id', 'ID')
            ->render(function (Purchase $purchase) {
                return Link::make($purchase->id)
                    ->route('platform.purchase.edit', $purchase->id);
            }),
            TD::set('user_id', 'User ID'),
            TD::set('game_id', 'Game ID'),
            TD::set('date_of_purchase', 'Date of Purchase'),
        ];
    }
}
