<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Screen\Actions\Link;
use App\Models\GameStudio;

class GameStudioListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'gameStudios';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id', 'ID')
            ->render(function (GameStudio $gameStudio) {
                return Link::make($gameStudio->id)
                    ->route('platform.gameStudio.edit', $gameStudio->id);
            }),
            TD::set('title', 'Title'),
            TD::set('game_id', 'Game ID'),
            TD::set('about', 'About')
                ->width('250px'),
        ];
    }
}
