<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Screen\Actions\Link;
use App\Models\Developer;

class DeveloperListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'developers';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id', 'ID')
            ->render(function (Developer $developer) {
                return Link::make($developer->id)
                    ->route('platform.developer.edit', $developer->id);
            }),
            TD::set('name','Name'),
            TD::set('about','About')
                ->width('300px'),
            TD::set('email','Email'),
        ];
    }
}
