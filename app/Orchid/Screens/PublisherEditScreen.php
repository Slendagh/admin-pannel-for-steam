<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Publisher;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Fields\Relation;

class PublisherEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Publisher';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Create New Publisher';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Publisher $publisher): array
    {
        $this->exists = $publisher->exists;

        if($this->exists){
            $this->description = 'Edit Publisher';
        }
        return [
            'id' => $publisher->id,
            'publisher' => Publisher::find($publisher->id),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Submit')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('publisher.name')
                    ->title('Name')
                    ->required(),
                TextArea::make('publisher.about')
                    ->title('About')
                    ->required(),
                Input::make('publisher.email')
                    ->title('email')
                    ->type('email')
                    ->required(),
            ])
        ];
    }

    /**
     * @param Publisher    $publisher
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Publisher $publisher, Request $request)
    {
       //dd($request->get('publisher'));
        $publisher->fill($request->get('publisher'))->save();
       // if($this->exists){
            Alert::info('You have successfully updated the Publisher.');
        /*}
        else{
            Alert::info('You have successfully created an publisher.');
        }*/
        return redirect()->route('platform.publisher.list');
    }

    /**
     * @param Publisher $publisher
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Publisher $publisher)
    {
        $publisher->delete()
            ? Alert::info('You have successfully deleted the Publisher.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.publisher.list');
    }
}
