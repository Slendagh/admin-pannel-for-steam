<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Badge;
use App\Orchid\Layouts\BadgeListLayout;
use App\Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\Button;
use Illuminate\Http\Request;

class BadgeListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'BadgesListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'BadgeListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
       // dd(Badge::find(1));

        return [
            'badge' => Badge::find(1),
            'badges' => Badge::paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create new Badge')
            ->icon('pencil')
            ->method('create'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            BadgeListLayout::class
        ];
    }

    public function create()
    {
        return redirect()->route('platform.badge.edit');
    }
}
