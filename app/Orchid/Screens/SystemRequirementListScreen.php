<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\SystemRequirement;
use App\Orchid\Layouts\SystemRequirementListLayout;
use App\Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\Button;
use Illuminate\Http\Request;



class SystemRequirementListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'SystemRequirementListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'SystemRequirementListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'systemRequirements' => SystemRequirement::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create new System Requirement')
                ->icon('pencil')
                ->method('create'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            SystemRequirementListLayout::class
        ];
    }

    public function create()
    {
        return redirect()->route('platform.systemRequirement.edit');
    }
}
