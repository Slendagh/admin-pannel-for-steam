<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\AgeRating;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Fields\Cropper;


class AgeRatingEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Age Rating';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Create a new Age Rating';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(AgeRating $AgeRating): array
    {
        $this->exists = $AgeRating->exists;

        if($this->exists){
            $creation = false;
            $this->description = 'Edit Age Rating';
        }
        return [
            'id' => $AgeRating->id,
            'AgeRating' => AgeRating::find($AgeRating->id),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Submit')
            ->icon('pencil')
            ->method('createOrUpdate')
            ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('AgeRating.name')
                    ->title('Name')
                    ->required()
                    ->placeholder('Entre a name'),

                TextArea::make('AgeRating.description')
                    ->title('Description')
                    ->rows(3)
                    ->required()
                    ->maxlength(200)
                    ->placeholder('Description for the age_rating'),
                Cropper::make('AgeRating.icon')
                    ->title('An icon for the age rating'),
            ])
        ];
    }

     /**
     * @param AgeRating    $AgeRating
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(AgeRating $AgeRating, Request $request)
    {
       // dd($request->get('AgeRating'));
        $AgeRating->fill(['name' => $request->get('AgeRating')['name'], 
                        'description' => $request->get('AgeRating')['description'],
                        'icon' => $request->get('AgeRating')['icon']])->save();
       // if($this->exists){
            Alert::info('You have successfully updated the AgeRating.');
        /*}
        else{
            Alert::info('You have successfully created an AgeRating.');
        }*/
        return redirect()->route('platform.agerating.list');
    }

    /**
     * @param AgeRating $AgeRating
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(AgeRating $AgeRating)
    {
        $AgeRating->delete()
            ? Alert::info('You have successfully deleted the AgeRating.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.agerating.list');
    }
}
