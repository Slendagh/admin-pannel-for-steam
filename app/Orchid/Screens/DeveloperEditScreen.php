<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Developer;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Alert;

class DeveloperEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Developer';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Create a new Developer';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Developer $developer): array
    {
        $this->exists = $developer->exists;

        if($this->exists){
            $this->description = 'Edit developer';
        }
        return [
            'id' => $developer->id,
            'developer' => Developer::find($developer->id),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Submit')
            ->icon('pencil')
            ->method('createOrUpdate')
            ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('developer.name')
                    ->title('Name')
                    ->required()
                    ->placeholder('Entre a developer Name'),
                TextArea::make('developer.about')
                    ->title('About')
                    ->required()
                    ->placeholder('Entre a Brief Biography Name'),
                Input::make('developer.email')
                    ->title('Email')
                    ->type('email')
                    ->required()
                    ->placeholder('Entre an Email'),
            ])
        ];
    }
    
    /**
     * @param Developer    $developer
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Developer $developer, Request $request)
    {
       //dd($request->get('developer'));
        $developer->fill($request->get('developer'))->save();
       // if($this->exists){
            Alert::info('You have successfully updated the developer.');
        /*}
        else{
            Alert::info('You have successfully created an developer.');
        }*/
        return redirect()->route('platform.developer.list');
    }

    /**
     * @param Developer $developer
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Developer $developer)
    {
        $developer->delete()
            ? Alert::info('You have successfully deleted the developer.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.developer.list');
    }
}
