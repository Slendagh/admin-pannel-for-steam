<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Friend;
use App\Orchid\Layouts\FriendListLayout;
use App\Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\Button;
use Illuminate\Http\Request;

class FriendListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'FriendListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'FriendListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'friends' => Friend::paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create new Friend')
                ->icon('pencil')
                ->method('create'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            FriendListLayout::class
        ];
    }

    public function create()
    {
        return redirect()->route('platform.friend.edit');
    }
}
