<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\GameStudio;
use App\Orchid\Layouts\GameStudioListLayout;
use App\Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\Button;
use Illuminate\Http\Request;

class GameStudioListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'GameStudioListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'GameStudioListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'gameStudios' => GameStudio::paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create new Game Studio')
                ->icon('pencil')
                ->method('create'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            GameStudioListLayout::class
        ];
    }

    public function create()
    {
        return redirect()->route('platform.gameStudio.edit');
    }
}
