<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Game;
use App\Orchid\Layouts\GameListLayout;
use App\Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\Button;
use Illuminate\Http\Request;

class GameListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Games List';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'List of all games. Click on ID of the game you want to modify/delete';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'game' => Game::find(1),
            'games' => Game::paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create new Game')
                ->icon('pencil')
                ->method('create'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            GameListLayout::class
        ];
    }

    public function create()
    {
        return redirect()->route('platform.game.edit');
    }
}
