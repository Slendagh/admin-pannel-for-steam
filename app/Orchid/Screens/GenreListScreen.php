<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Genre;
use App\Orchid\Layouts\GenreListLayout;
use App\Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\Button;
use Illuminate\Http\Request;

class GenreListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'GenreListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'GenreListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'genres' => Genre::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create new Genre')
                ->icon('pencil')
                ->method('create'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            GenreListLayout::class
        ];
    }

    public function create()
    {
        return redirect()->route('platform.genre.edit');
    }
}
