<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Review;
use App\Orchid\Layouts\ReviewListLayout;
use App\Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\Button;
use Illuminate\Http\Request;

class ReviewListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'ReviewListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'ReviewListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'reviews' => Review::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create new Review')
                ->icon('pencil')
                ->method('create'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            ReviewListLayout::class
        ];
    }

    public function create()
    {
        return redirect()->route('platform.review.edit');
    }
}
