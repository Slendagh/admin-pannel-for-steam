<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Tutorial;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Alert;


class TutorialEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'TutorialEditScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'TutorialEditScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Tutorial $tutorial): array
    {
        return [
            'tutorial' => Tutorial::find($tutorial->id)
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('submit')
                ->icon('pencil')
                ->method('update'),
            Button::make('remove')
                ->icon('trash')
                ->method('remove'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('tutorial.name')
                    ->title('Name')
                    ->required(),
                Input::make('tutorial.username')
                    ->title('Username')
                    ->required(),
                Input::make('tutorial.email')
                    ->title('Email')
                    ->type('email')
                    ->required(),
            ])
        ];
    }


    /**
     * @param Tutorial    $tuto
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */

    public function update(Tutorial $tuto, Request $request){
        $tuto->fill($request->get('tutorial'))->save();
        Alert::info('You have successfully updated the Tutorial');

        return redirect()->route('platform.tutorial.list');
    }


    /**
     * @param Tutorial    $tuto
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */

    public function remove(Tutorial $tuto){
        $tuto->delete()
            ? Alert::info('You have successfully deleted the Tutorial')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.tutorial.list');
    }
}
