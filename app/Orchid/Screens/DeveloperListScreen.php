<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Developer;
use App\Orchid\Layouts\DeveloperListLayout;
use App\Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\Button;
use Illuminate\Http\Request;

class DeveloperListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'DeveloperListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'DeveloperListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'developers' => Developer::paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create new Developer')
            ->icon('pencil')
            ->method('create'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            DeveloperListLayout::class
        ];
    }

    public function create()
    {
        return redirect()->route('platform.developer.edit');
    }
}
