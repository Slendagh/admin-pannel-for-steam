<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Purchase;
use App\Models\Game;
use App\Models\User;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\DateTimer;


class PurchaseEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Purchase';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Create a new Purchase';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Purchase $purchase): array
    {
        $this->exists = $purchase->exists;

        if($this->exists){
            $this->description = 'Edit Purchase';
        }
        return [
            'id' => $purchase->id,
            'purchase' => Purchase::find($purchase->id),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Submit')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Relation::make('purchase.user_id')
                    ->title('User Name')
                    ->fromModel(User::class, 'name')
                    ->required(),
                Relation::make('purchase.game_id')
                    ->title('Game Name')
                    ->fromModel(Game::class, 'title')
                    ->required(),
                DateTimer::make('purchase.date_of_purchase')
                    ->title('Date of Purchase')
                    ->required(),
            ])
        ];
    }

    /**
     * @param Purchase    $purchase
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Purchase $purchase, Request $request)
    {
       //dd($request->get('purchase'));
        $purchase->fill($request->get('purchase'))->save();
       // if($this->exists){
            Alert::info('You have successfully updated the Purchase.');
        /*}
        else{
            Alert::info('You have successfully created an purchase.');
        }*/
        return redirect()->route('platform.purchase.list');
    }

    /**
     * @param Purchase $purchase
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Purchase $purchase)
    {
        $purchase->delete()
            ? Alert::info('You have successfully deleted the Purchase.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.purchase.list');
    }
}
