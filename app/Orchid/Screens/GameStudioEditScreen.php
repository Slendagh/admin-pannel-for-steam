<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\GameStudio;
use App\Models\Game;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Fields\Relation;

class GameStudioEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Game Studio';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Create new Game Studio';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(GameStudio $gameStudio): array
    {
        $this->exists = $gameStudio->exists;

        if($this->exists){
            $this->description = 'Edit Game Studio';
        }
        return [
            'id' => $gameStudio->id,
            'gameStudio' => GameStudio::find($gameStudio->id),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Submit')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('gameStudio.title')
                    ->title('Title')
                    ->required(),
                Relation::make('gameStudio.game_id')
                    ->title('Game Name')
                    ->required()
                    ->fromModel(Game::class, 'title'),
                Textarea::make('gameStudio.about')
                    ->title('About')
                    ->required(),
            ])
        ];
    }

    /**
     * @param GameStudio    $gameStudio
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(GameStudio $gameStudio, Request $request)
    {
       //dd($request->get('gameStudio'));
        $gameStudio->fill($request->get('gameStudio'))->save();
       // if($this->exists){
            Alert::info('You have successfully updated the Game Studio.');
        /*}
        else{
            Alert::info('You have successfully created an gameStudio.');
        }*/
        return redirect()->route('platform.gameStudio.list');
    }

    /**
     * @param GameStudio $gameStudio
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(GameStudio $gameStudio)
    {
        $gameStudio->delete()
            ? Alert::info('You have successfully deleted the Game Studio.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.gameStudio.list');
    }
}
