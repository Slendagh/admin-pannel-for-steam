<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Tag;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Alert;

class TagEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Tag';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Create a new Tag';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Tag $tag): array
    {
        $this->exists = $tag->exists;

        if($this->exists){
            $this->description = 'Edit Tag';
        }
        return [
            'id' => $tag->id,
            'tag' => Tag::find($tag->id),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Submit')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('tag.name')
                    ->title('Tag Name')
                    ->required(),
            ])
        ];
    }

    /**
     * @param Tag    $tag
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Tag $tag, Request $request)
    {
       //dd($request->get('tag'));
        $tag->fill($request->get('tag'))->save();
       // if($this->exists){
            Alert::info('You have successfully updated the Tag.');
        /*}
        else{
            Alert::info('You have successfully created an tag.');
        }*/
        return redirect()->route('platform.tag.list');
    }

    /**
     * @param Tag $tag
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Tag $tag)
    {
        $tag->delete()
            ? Alert::info('You have successfully deleted the Tag.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.tag.list');
    }
}
