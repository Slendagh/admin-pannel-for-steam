<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\UserBadgesActivity;
use App\Orchid\Layouts\UserBadgeActivityListLayout;
use App\Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\Button;
use Illuminate\Http\Request;

class UserBadgeActivityListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'UserBadgeActivityListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'UserBadgeActivityListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'activities' => UserBadgesActivity::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create new User BadgeActivity')
                ->icon('pencil')
                ->method('create'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            UserBadgeActivityListLayout::class
        ];
    }

    public function create()
    {
        return redirect()->route('platform.userBadgeActivity.edit');
    }
}
