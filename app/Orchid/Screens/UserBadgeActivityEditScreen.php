<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\UserBadgesActivity;
use App\Models\Badge;
use App\Models\User;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\DateTimer;

class UserBadgeActivityEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'User Badge Activity';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'create a new Activity';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(UserBadgesActivity $activity): array
    {
        $this->exists = $activity->exists;

        if($this->exists){
            $this->description = 'Edit User Badge Activity';
        }
        return [
            'id' => $activity->id,
            'activity' => UserBadgesActivity::find($activity->id),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Submit')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Relation::make('activity.user_id')
                    ->title('User Name')
                    ->fromModel(User::class, 'name')
                    ->required(),
                Relation::make('activity.badge_id')
                    ->title('Badge Name')
                    ->fromModel(Badge::class, 'name')
                    ->required(),
                DateTimer::make('activity.date_of_unlock')
                    ->title('Date of Unlock')
                    ->required(),
            ])
        ];
    }

    /**
     * @param UserBadgesActivity    $userBadgeActivity
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(UserBadgesActivity $userBadgeActivity, Request $request)
    {
       //dd($request->get('userBadgeActivity'));
        $userBadgeActivity->fill($request->get('activity'))->save();
       // if($this->exists){
            Alert::info('You have successfully updated the userBadgeActivity.');
        /*}
        else{
            Alert::info('You have successfully created an userBadgeActivity.');
        }*/
        return redirect()->route('platform.userBadgeActivity.list');
    }

    /**
     * @param UserBadgesActivity $userBadgeActivity
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(UserBadgesActivity $userBadgeActivity)
    {
        $userBadgeActivity->delete()
            ? Alert::info('You have successfully deleted the userBadgeActivity.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.userBadgeActivity.list');
    }
}
