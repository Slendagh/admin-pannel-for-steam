<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\AgeRating;
use App\Orchid\Layouts\AgeRatingListLayout;
use App\Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\Button;
use Illuminate\Http\Request;


class AgeRatingListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'AgeRatingListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'AgeRatingListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'AgeRating' => AgeRating::find(1),
            'AgeRatings' => AgeRating::paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create new Age_Rating')
            ->icon('pencil')
            ->method('create'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
           AgeRatingListLayout::class
        ];
    }

    public function create()
    {
        return redirect()->route('platform.agerating.edit');
    }
}