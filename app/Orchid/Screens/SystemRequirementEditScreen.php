<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\SystemRequirement;
use App\Models\Game;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Fields\Relation;


class SystemRequirementEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'System Requirement';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Create a new System Requirement';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(SystemRequirement $systemRequirement): array
    {
            $this->exists = $systemRequirement->exists;

            if($this->exists){
                $this->description = 'Edit System Requirement';
            }
            return [
                'id' => $systemRequirement->id,
                'systemRequirement' => SystemRequirement::find($systemRequirement->id),
            ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Submit')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Relation::make('systemRequirement.game_id')
                    ->title('Game Name')
                    ->fromModel(Game::class, 'title')
                    ->required(),
                Input::make('systemRequirement.storage_KB')
                    ->title('KB Storage needed')
                    ->type('number')
                    ->required(),
                
            ])
        ];
    }

    /**
     * @param SystemRequirement    $systemRequirement
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(SystemRequirement $systemRequirement, Request $request)
    {
       //dd($request->get('SystemRequirement'));
        $systemRequirement->fill($request->get('systemRequirement'))->save();
       // if($this->exists){
            Alert::info('You have successfully updated the System Requirement.');
        /*}
        else{
            Alert::info('You have successfully created an System Requirement.');
        }*/
        return redirect()->route('platform.systemRequirement.list');
    }

    /**
     * @param SystemRequirement $systemRequirement
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(SystemRequirement $systemRequirement)
    {
        $systemRequirement->delete()
            ? Alert::info('You have successfully deleted the System Requirement.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.systemRequirement.list');
    }
}
