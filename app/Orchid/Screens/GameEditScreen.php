<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Game;
use App\Models\User;
use App\Models\Genre;
use App\Models\Developer;
use App\Models\Publisher;
use App\Models\AgeRating;
use App\Models\Tag;
use App\Models\Category;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Fields\DateTimer;

class GameEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create new Game';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'GameEditScreen';

    /**
     * Display header description.
     *
     * @var bool
     */
    public $creation = true;//not working

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Game $game): array
    {
        $this->exists = $game->exists;

        if($this->exists){
            $creation = false;
            $this->name = 'Edit game';
        }
        return [
            'id' => $game->id,
            'game' => Game::find($game->id),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Submit')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout():array
    {
        return [
            Layout::rows([
                Input::make('game.title')
                    ->title('Title')
                    ->placeholder('Attractive but mysterious title')
                    ->help('Specify a short descriptive title for this game.'),

                TextArea::make('game.description')
                    ->title('Description')
                    ->rows(3)
                    ->maxlength(200)
                    ->placeholder('Brief description for preview'),

                Relation::make('game.genre_id')
                    ->title('genre')
                    ->required()
                    ->fromModel(Genre::class, 'name'),

                Relation::make('game.tag_id')
                    ->title('tag')
                    ->required()
                    ->fromModel(Tag::class, 'name'),

                Relation::make('game.category_id')
                    ->title('category')
                    ->required()
                    ->fromModel(Category::class, 'name'),

                Relation::make('game.age_rating_id')
                    ->title('age rating')
                    ->required()
                    ->fromModel(AgeRating::class, 'name'),

                Relation::make('game.publisher_id')
                    ->title('publisher')
                    ->required()
                    ->fromModel(Publisher::class, 'name'),

                Relation::make('game.developer_id')
                    ->title('developer')
                    ->required()
                    ->fromModel(Developer::class, 'name'),

                Input::make('game.rating')
                    ->title('rating')
                    ->required()
                    ->placeholder('enter game rating')
                    ->type('range')->min(0)->max(5),

                DateTimer::make('game.release_date')
                    ->required()
                    ->title('Release Date'),
                /*Upload::make('game.default_image')
                    ->title('default image for the game'),*/
            ])
        ];
    }

    /**
     * @param Game    $game
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Game $game, Request $request)
    {
        $game->fill($request->get('game'))->save();
       // if($this->exists){
            Alert::info('You have successfully updated the game.');
        /*}
        else{
            Alert::info('You have successfully created an game.');
        }*/
        return redirect()->route('platform.game.list');
    }

    /**
     * @param Game $game
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Game $game)
    {
        $game->delete()
            ? Alert::info('You have successfully deleted the game.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.game.list');
    }
}
