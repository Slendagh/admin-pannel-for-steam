<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Category;
use App\Orchid\Layouts\CategoryListLayout;
use App\Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\Button;
use Illuminate\Http\Request;


class CategoryListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'CategoryListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'CategoryListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        //dd(Category::paginate());
        return [
            'categories' => Category::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create new Category')
            ->icon('pencil')
            ->method('create'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            CategoryListLayout::class
        ];
    }

    public function create()
    {
        return redirect()->route('platform.category.edit');
    }
}
