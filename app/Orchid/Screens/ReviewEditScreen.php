<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Review;
use App\Models\Game;
use App\Models\User;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Fields\Relation;

class ReviewEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Review';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Create a new Review';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Review $review): array
    {
        $this->exists = $review->exists;

        if($this->exists){
            $this->description = 'Edit review';
        }
        return [
            'id' => $review->id,
            'review' => Review::find($review->id),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Submit')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Relation::make('review.user_id')
                    ->title('User Name')
                    ->fromModel(User::class, 'name')
                    ->required(),
                Relation::make('review.game_id')
                    ->title('Game Name')
                    ->fromModel(Game::class, 'title')
                    ->required(),
                TextArea::make('review.review')
                    ->title('Review')
                    ->required(),
            ])
        ];
    }

    /**
     * @param Review    $review
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Review $review, Request $request)
    {
       //dd($request->get('review'));
        $review->fill($request->get('review'))->save();
       // if($this->exists){
            Alert::info('You have successfully updated the review.');
        /*}
        else{
            Alert::info('You have successfully created an review.');
        }*/
        return redirect()->route('platform.review.list');
    }

    /**
     * @param Review $review
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Review $review)
    {
        $review->delete()
            ? Alert::info('You have successfully deleted the review.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.review.list');
    }
}
