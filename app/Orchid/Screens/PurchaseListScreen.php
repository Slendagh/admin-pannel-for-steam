<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Purchase;
use App\Orchid\Layouts\PurchaseListLayout;
use App\Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\Button;
use Illuminate\Http\Request;

class PurchaseListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Purchase Screen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'purchases' => Purchase::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create new Purchase')
                ->icon('pencil')
                ->method('create'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            PurchaseListLayout::class
        ];
    }

    public function create()
    {
        return redirect()->route('platform.purchase.edit');
    }
}
