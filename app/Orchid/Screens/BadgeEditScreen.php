<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Badge;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Fields\Cropper;

class BadgeEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Badge';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Create new Badge';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Badge $badge): array
    {
            $this->exists = $badge->exists;

            if($this->exists){
                $creation = false;
                $this->description = 'Edit Badge';
            }
            return [
                'id' => $badge->id,
                'badge' => badge::find($badge->id),
            ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Submit')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('badge.name')
                    ->title('Name')
                    ->required()
                    ->placeholder('Entre a name'),
                
                Input::make('badge.xp')
                    ->title('XP')
                    ->type('number')
                    ->required()
                    ->placeholder('Entre xp needed'),

                TextArea::make('badge.description')
                    ->title('Description')
                    ->rows(3)
                    ->required()
                    ->maxlength(200)
                    ->placeholder('Description for the age_rating'),
                Cropper::make('badge.icon')
                    ->title('An icon for the age rating'),
            ])
        ];
    }

     /**
     * @param Badge    $badge
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Badge $badge, Request $request)
    {
       // dd($request->get('badge'));
        $badge->fill(['name' => $request->get('badge')['name'], 
                        'xp' => $request->get('badge')['xp'],
                        'description' => $request->get('badge')['description'],
                        'icon' => $request->get('badge')['icon']])->save();
       // if($this->exists){
            Alert::info('You have successfully updated the badge.');
        /*}
        else{
            Alert::info('You have successfully created an badge.');
        }*/
        return redirect()->route('platform.badge.list');
    }

    /**
     * @param Badge $badge
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Badge $badge)
    {
        $badge->delete()
            ? Alert::info('You have successfully deleted the badge.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.badge.list');
    }
}
