<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Friend;
use App\Models\User;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Fields\Relation;

class FriendEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Friend';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Create new Friend';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Friend $friend): array
    {
        $this->exists = $friend->exists;

        if($this->exists){
            $this->description = 'Edit friend';
        }
        return [
            'id' => $friend->id,
            'friend' => Friend::find($friend->id),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Submit')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Relation::make('friend.user1_id')
                    ->title('User 1')
                    ->required()
                    ->fromModel(User::class, 'name'),
                Relation::make('friend.user2_id')
                    ->title('User 2')
                    ->required()
                    ->fromModel(User::class, 'name'),
            ])
        ];
    }

    /**
     * @param Friend    $friend
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Friend $friend, Request $request)
    {
       //dd($request->get('friend'));
        $friend->fill($request->get('friend'))->save();
       // if($this->exists){
            Alert::info('You have successfully updated the friend.');
        /*}
        else{
            Alert::info('You have successfully created an friend.');
        }*/
        return redirect()->route('platform.friend.list');
    }

    /**
     * @param Friend $friend
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Friend $friend)
    {
        $friend->delete()
            ? Alert::info('You have successfully deleted the friend.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.friend.list');
    }
}
