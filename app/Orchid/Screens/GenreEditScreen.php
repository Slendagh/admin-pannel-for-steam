<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Genre;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Fields\Relation;

class GenreEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Genre';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Create new Genre';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Genre $genre): array
    {
            $this->exists = $genre->exists;

            if($this->exists){
                $this->description = 'Edit Game Studio';
            }
            return [
                'id' => $genre->id,
                'genre' => Genre::find($genre->id),
            ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Submit')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('genre.name')
                    ->title('Name')
                    ->required(),
            ])
        ];
    }

    /**
     * @param Genre    $genre
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Genre $genre, Request $request)
    {
       //dd($request->get('genre'));
        $genre->fill($request->get('genre'))->save();
       // if($this->exists){
            Alert::info('You have successfully updated the Game Studio.');
        /*}
        else{
            Alert::info('You have successfully created an genre.');
        }*/
        return redirect()->route('platform.genre.list');
    }

    /**
     * @param Genre $genre
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Genre $genre)
    {
        $genre->delete()
            ? Alert::info('You have successfully deleted the Game Studio.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.genre.list');
    }
}
