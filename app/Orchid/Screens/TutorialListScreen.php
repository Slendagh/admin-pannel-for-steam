<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Tutorial;
use App\Orchid\Layouts\TutorialListLayout;

class TutorialListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'TutorialListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'TutorialListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'tutorials' => Tutorial::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            TutorialListLayout::class
        ];
    }
}
