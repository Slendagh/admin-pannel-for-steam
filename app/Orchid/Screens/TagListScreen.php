<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Models\Tag;
use App\Orchid\Layouts\TagListLayout;
use App\Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\Button;
use Illuminate\Http\Request;

class TagListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'TagListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'TagListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'tags' => Tag::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create new Tag')
                ->icon('pencil')
                ->method('create'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            TagListLayout::class
        ];
    }

    public function create()
    {
        return redirect()->route('platform.tag.edit');
    }
}
