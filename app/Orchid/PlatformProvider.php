<?php

namespace App\Orchid;

use Laravel\Scout\Searchable;
use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemMenu;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\OrchidServiceProvider;

class PlatformProvider extends OrchidServiceProvider
{
    /**
     * @param Dashboard $dashboard
     */
    public function boot(Dashboard $dashboard): void
    {
        parent::boot($dashboard);

        // ...
    }

    /**
     * @return ItemMenu[]
     */
    public function registerMainMenu(): array
    {
        return [
        //start of navigation
            ItemMenu::label('Example screen')
                ->icon('monitor')
                ->route('platform.example')
                ->title('Navigation')
                ->badge(function () {
                    return 6;
                }),

            //user screen
            ItemMenu::label('User Screen')
                ->icon('user')
                ->route('platform.systems.users'),
                
            //game screen
            ItemMenu::label('Game Screen')
                ->icon('game-controller')
                ->route('platform.game.list'),
            
            //Age Rating screen
            ItemMenu::label('Age Rating Screen')
                ->icon('options-vertical')
                ->route('platform.agerating.list'),

             //Badge screen
             ItemMenu::label('Badge Screen')
                ->icon('badge')
                ->route('platform.badge.list'),

             //Category screen
             ItemMenu::label('Category Screen')
                ->icon('drawer')
                ->route('platform.category.list'),
            
             //Developer screen
             ItemMenu::label('Developer Screen')
                ->icon('screen-desktop')
                ->route('platform.developer.list'),

             //Friend screen
             ItemMenu::label('Friend Screen')
                ->icon('friends')
                ->route('platform.friend.list'),
             
             //GameStudio screen
             ItemMenu::label('Game Studios Screen')
                ->icon('building')
                ->route('platform.gameStudio.list'),

             //Genre screen
             ItemMenu::label('Genre Screen')
                ->icon('folder')
                ->route('platform.genre.list'),

             //Publisher screen
             ItemMenu::label('Publisher Screen')
                ->icon('save-alt')
                ->route('platform.publisher.list'),

             //Purchase screen
             ItemMenu::label('Purchase Screen')
                ->icon('credit-card')
                ->route('platform.purchase.list'),

            //Review screen
            ItemMenu::label('Review Screen')
                ->icon('bubble')
                ->route('platform.review.list'),

            //System Requirement screen
            ItemMenu::label('System Requirement Screen')
                ->icon('settings')
                ->route('platform.systemRequirement.list'),
            
            //Tag screen
            ItemMenu::label('Tag Screen')
                ->icon('badge')
                ->route('platform.tag.list'),

            //User Badge Activity screen
            ItemMenu::label('User Badge Activity Screen')
                ->icon('config')
                ->route('platform.userBadgeActivity.list'),

            //User Badge Activity screen
            ItemMenu::label('Tutorial Screen')
            ->icon('start')
            ->route('platform.tutorial.list'),
            


        //end of navigation

            ItemMenu::label('Basic Elements')
                ->title('Form controls')
                ->icon('note')
                ->route('platform.example.fields'),

            ItemMenu::label('Advanced Elements')
                ->icon('briefcase')
                ->route('platform.example.advanced'),

            ItemMenu::label('Text Editors')
                ->icon('list')
                ->route('platform.example.editors'),

            ItemMenu::label('Overview layouts')
                ->title('Layouts')
                ->icon('layers')
                ->route('platform.example.layouts'),

            ItemMenu::label('Chart tools')
                ->icon('bar-chart')
                ->route('platform.example.charts'),

            ItemMenu::label('Cards')
                ->icon('grid')
                ->route('platform.example.cards'),

            ItemMenu::label('Documentation')
                ->title('Docs')
                ->icon('docs')
                ->url('https://orchid.software/en/docs'),
        ];
    }

    /**
     * @return ItemMenu[]
     */
    public function registerProfileMenu(): array
    {
        return [
            ItemMenu::label('Profile')
                ->route('platform.profile')
                ->icon('user'),
        ];
    }

    /**
     * @return ItemMenu[]
     */
    public function registerSystemMenu(): array
    {
        return [
            ItemMenu::label(__('Access rights'))
                ->icon('lock')
                ->slug('Auth')
                ->active('platform.systems.*')
                ->permission('platform.systems.index')
                ->sort(1000),

            ItemMenu::label(__('Users'))
                ->place('Auth')
                ->icon('user')
                ->route('platform.systems.users')
                ->permission('platform.systems.users')
                ->sort(1000)
                ->title(__('All registered users')),

            ItemMenu::label(__('Roles'))
                ->place('Auth')
                ->icon('lock')
                ->route('platform.systems.roles')
                ->permission('platform.systems.roles')
                ->sort(1000)
                ->title(__('A Role defines a set of tasks a user assigned the role is allowed to perform. ')),
        ];
    }

    /**
     * @return ItemPermission[]
     */
    public function registerPermissions(): array
    {
        return [
            ItemPermission::group(__('Systems'))
                ->addPermission('platform.systems.roles', __('Roles'))
                ->addPermission('platform.systems.users', __('Users')),
        ];
    }

    /**
     * @return Searchable|string[]
     */
    public function registerSearchModels(): array
    {
        return [
            // ...Models
            // \App\Models\User::class
        ];
    }
}
