<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;
use Orchid\Attachment\Attachable;
use Orchid\Attachment\Models\Attachment;

class Genre extends Model
{
    use HasFactory;
    use AsSource, Attachable;

    protected $fillable = [
        'name',
    ];
}
