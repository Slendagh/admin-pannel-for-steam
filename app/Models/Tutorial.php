<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class Tutorial extends Model
{
    use HasFactory, asSource;

    protected $fillable = [
        'name', 
        'email',
        'username'
    ];
}
