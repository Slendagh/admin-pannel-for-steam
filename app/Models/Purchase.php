<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class Purchase extends Model
{
    use HasFactory, AsSource;

    protected $fillable = [
        'user_id',
        'game_id',
        'date_of_purchase'
    ];
}
