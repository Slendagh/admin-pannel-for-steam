<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class Review extends Model
{
    use HasFactory, AsSource;

    protected $fillable = [
        'user_id',
        'game_id',
        'review'
    ];
}
