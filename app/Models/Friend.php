<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;
use Orchid\Attachment\Attachable;
use Orchid\Attachment\Models\Attachment;

class Friend extends Model
{
    use HasFactory;
    use AsSource, Attachable;

    protected $fillable = [
        'user1_id',
        'user2_id',
    ];
}
