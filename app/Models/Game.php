<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;
use Orchid\Attachment\Attachable;
use Orchid\Attachment\Models\Attachment;

class Game extends Model
{
    use HasFactory;
    use AsSource, Attachable;

    protected $fillable = [
        'title',
        'genre_id',
        'category_id',
        'tag_id',
        'description',
        'age_rating_id',
        'rating',
        'publisher_id',
        'developer_id',
        'release_date',
        //'default_image',
    ];
}
