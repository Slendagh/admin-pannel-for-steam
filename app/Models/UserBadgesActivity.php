<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class UserBadgesActivity extends Model
{
    use HasFactory, AsSource;

    protected $fillable = [
        'badge_id',
        'date_of_unlock',
        'user_id'
    ];
}
