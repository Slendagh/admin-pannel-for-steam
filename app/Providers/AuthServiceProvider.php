<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerDeveloperPolicies();
        //
    }

    public function registerDeveloperPolicies(){

        Gate::define('edit-ones-games', function($user){
            $user->hasAccess(['edit-ones-games']);
        });
        Gate::define('list-ones-games', function($user){
            $user->hasAccess(['list-ones-games']);
        });
        Gate::define('list-ones-game-reviews', function($user){
            $user->hasAccess(['list-ones-game-reviews']);
        });
    }
}
